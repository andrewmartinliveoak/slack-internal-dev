import * as AWS from 'aws-sdk';
import * as yaml from 'js-yaml';
import * as crypto from 'crypto';

class Handler {
    public mergeWho(config: any): any {
        let shouldSave = false;
        let d = new Date();
        if (d >= new Date(config.nextChange)) {
            config.currentPersonIndex = (config.currentPersonIndex + 1) % config.people.length;
            config.nextChange = this.getNextDayOfWeek(d, 1).toISOString().substring(0, 10);
            shouldSave = true;
        }
        return {
            shouldSave: shouldSave, config: config
        };
    };

    private getNextDayOfWeek(d: any, dayOfWeek: any): Date {
        const dateDay = d.getDay();
        let daysToAdd = 0;
        if (dateDay === dayOfWeek) {
            daysToAdd = 7;
        } else {
            daysToAdd = (7 + dayOfWeek - dateDay) % 7;
        }
        let resultDate = new Date();
        resultDate.setDate(d.getDate() + daysToAdd);
        return resultDate;
    };
}

export const mergeWhoMS = (event: any, context: any, cb: any) => {
    const sharedSecret = <string>process.env.TOKEN;

    if (isAuthorized(event.headers["Authorization"], sharedSecret, event.body)) {
        processRequest(cb);
    } else {
        executeCallback(cb, 401, JSON.stringify({
            type: "text",
            text: "Unauthorized"
        }));
    }
};

export const mergeWho = (event: any, context: any, cb: any) => {
    if (!event.body.includes(<string>process.env.TOKEN)) {
        executeCallback(cb, 401, 'null');
    }
    processRequest(cb);
}

const isAuthorized = function (authHeader: any, secretKey: string, content: string) {
    if (authHeader) {
        const retrievedSignature = authHeader.replace("HMAC ", "");
        const computedSignature = crypto.createHmac("sha256", Buffer.from(secretKey, "base64")).update(content).digest("base64");
        if (computedSignature === retrievedSignature) {
            return true;
        }
    }

    return false;
}

const processRequest = function (cb: any) {
    const bucket = <string>process.env.BUCKET_NAME;
    const filePath = <string>process.env.CONFIG_FILE;

    const s3 = new AWS.S3();
    s3.getObject({ Bucket: bucket, Key: filePath }, function (err: any, data: any) {
        let handler = new Handler();
        if (err) {
            executeCallback(cb, 500, err);
        }
        else {
            const body = data.Body.toString('utf-8');
            const doc = getConfig(body);
            const result = handler.mergeWho(doc);
            const config = result.config;
            const captain = config.people[config.currentPersonIndex];
            const slackResponse = JSON.stringify({
                response_type: "in_channel",
                text: captain.name + ' is merge captain this week.'
            });

            executeCallback(cb, 200, slackResponse);

            if (result.shouldSave) {
                s3.putObject({ Bucket: bucket, Key: filePath, Body: saveConfig(config) }, function (err: any, data: any) {
                    if (err) {
                        executeCallback(cb, 500, err);
                    }
                });
            }
        }
    });
}

const executeCallback = function (cb: any, statusCode: number, body: string) {
    cb(null, {
        statusCode: statusCode,
        body: body
    });
};

const getConfig = function (data: any) {
    return yaml.safeLoad(data);
}

const saveConfig = function (config: any) {
    return yaml.safeDump(config);
}